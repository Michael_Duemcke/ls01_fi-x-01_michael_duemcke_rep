import java.util.Scanner;
public class Fahrkartenautomat {

	public static void main(String[] args) {
		while(true) {double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(r�ckgabebetrag);

		System.out.println("\nVergessen Sie nicht, die Fahrscheine\n" + "vor Fahrtantritt entwerten zu lassen!\n"
		+ "Wir w�nschen Ihnen eine gute Fahrt.");
		
		}
		}

		public static double fahrkartenbestellungErfassen()  {
			double einzelpreis_ticket=0.0;
			int[]auswahlnummer= {0,1,2,3,4,5,6,7,8,9,10};

			String[] fahrkartenbezeichnung = { 
					"Abbruch",
					"Einzelfahrschein Berlin AB", 
					"Einzelfahrschein Berlin BC", 
					"Einzelfahrschein Berlin ABC",
					"Kurzstrecke",
					"Tageskarte Berlin AB",
					"Tageskarte Berlin BC",
					"Tageskarte Berlin ABC",
					"Kleingruppen-Tageskarte Berlin AB",
					"Kleingruppen-Tageskarte Berlin BC",
					"Kleingruppen-Tageskarte Berlin ABC",
			};
			double[] fahrkartenpreis = {
					0.00,
					2.90, 
					3.30,
					3.60,
					1.90,
					8.60,
					9.00,
					9.60,
					23.50,
					24.30,
					24.90
			};
			
			System.out.printf("%-10s%40s%30s%n","Auswahlnummer","Bezeichnung","Preis in Euro");
			System.out.printf("%n%n");
			for(int m=0;m<auswahlnummer.length;m++) {
				System.out.printf("%-10s%40s%30s%n",auswahlnummer[m],fahrkartenbezeichnung[m],fahrkartenpreis[m]);
			}
			System.out.printf("%n%n%n");
			
			//Eingabe Ticket_wahl
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine der genannten Ticketauswahlen an: ");
		int ihre_Wahl= tastatur.nextInt();
		
		//Wahl wird ausgewertet
			while(ihre_Wahl > auswahlnummer.length || ihre_Wahl<1){
				 System.out.println(">>falsche Eingabe<<");
			ihre_Wahl=tastatur.nextInt();

			}
			//Eingabe Anzahl Tickets
		System.out.println("Wie viele Tickets werden gekauft?");
		double anzahlTickets = tastatur.nextInt();
		
		return fahrkartenpreis[ihre_Wahl] * anzahlTickets;

		}

		public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag)

		{
		System.out.printf("%s%.2f EURO %s", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag),
		"  ");
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
		}

		public static void fahrkartenAusgeben() {
		System.out.println("\nFahrscheine werden ausgegeben.");
		for (int i = 0; i < 8; i++) {
		System.out.print("=");
		try {
		Thread.sleep(250);
		} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
		System.out.println("\n\n");
		}

		public static void rueckgeldAusgeben(double r�ckgabebetrag) {
		if (r�ckgabebetrag > 0.0) {
		System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
		System.out.println("wird in folgenden M�nzen ausgezahlt:");

		while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
		{
		System.out.println("2 EURO");
		r�ckgabebetrag -= 2.0;
		}
		while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
		{
		System.out.println("1 EURO");
		r�ckgabebetrag -= 1.0;
		}
		while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
		{
		System.out.println("50 CENT");
		r�ckgabebetrag -= 0.5;
		}
		while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
		{
		System.out.println("20 CENT");
		r�ckgabebetrag -= 0.2;
		}
		while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
		{
		System.out.println("10 CENT");
		r�ckgabebetrag -= 0.1;
		}
		while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		{
		System.out.println("5 CENT");
		r�ckgabebetrag -= 0.05;
		}
		}
		}
	}
//Antwort zu Aufgabe 1:
//Wenn man die Fahrkartenbezeichnung und den Fahrkartenpreis in einem Array packt,
//dann hat es den Vorteil, das eine gute �bersichtlichkeit der ganzen Fahrkartenbezeichnungen und
//den ganzen dazugeh�rigen Ticketpreisen darbietet. Der 2. Vorteil ist, das man als Programmierer den Code
//leichter �berschauen bzw. leichter ver�ndern

//Antwort zu Aufgabe 3:
//Implementierung Array:
//Vorteil:
//1. bessere �bersichtlichkeit des Codes, v.a. f�r Programmierer
//
//Nachteil:
//1. bei sehr langen Arrays z.B. mehr als 100 Indizien k�nnte es passieren,
//dass der Code nicht mehr �bersichtlich wird
//
//Keine Implementierung Array:
//Vorteil:-
//Nachteil: viel l�nger Code => Un�bersichtlichkeit

