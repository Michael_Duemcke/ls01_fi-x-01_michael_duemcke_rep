﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag ;
       double eingeworfeneMünze;
       double rückgabebetrag;
       
       
       double ticketAnzahl;
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       

       System.out.print("Anzahl der Ticket: ");
       ticketAnzahl = fahrkartenbestellungErfassen();
       
       
      
     

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   
    	   System.out.printf("Noch zu zahlen: %.2f Euro %n ", + (zuZahlenderBetrag * - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
       }
System.out.printf("Liebe Person ihr Wechselgeld beträgt: %.2f", fahrkartenBezahlen(eingezahlterGesamtbetrag, zuZahlenderBetrag, ticketAnzahl));
       // Fahrscheinausgabe
       // -----------------
    fahrkartenAusgeben();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
/*Anmerkung: rückgabebetrag = Math.round(r�ckgabebetrag * 100)/100.0; =>
 * Dieser Zusatz soll falsche Rückgeldausgaben präventiv verhindern
 * */
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
	          rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
	          rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
	          rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
 	         rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
	          rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
 	         rückgabebetrag = Math.round(rückgabebetrag * 100)/100.0;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       //test
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner (System.in);
    	 System.out.print("Anzahl der Ticket: ");
         double ticket_anzahl = tastatur.nextDouble();
         
    	return ticket_anzahl;
    }
    public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double ticketAnzahl) 
    {
    	double zuZahlen = eingezahlterGesamtbetrag - zuZahlenderBetrag * ticketAnzahl;
    	return zuZahlen;
    }
    public static void fahrkartenAusgeben() {
    	 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
    	 {
	    	   System.out.print("=");
         try {
    		Thread.sleep(250);
    	} catch (InterruptedException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		System.out.println("\n\n");
    	}
    }
    System.out.println("\n\n");
    }
         public static double rueckgeldAusgeben(double rückgabebetrag)
         {
         	double zuZahlen = rückgabebetrag;
         	return zuZahlen;
         }
}