/**
 * @author Michael Duemcke
 * @since Ende April 2022
 * @version 1.0
 */
import java.util.ArrayList;
import java.util.*;
/**
 * 
 * Es die Klasse Raumschiff 
 * Am Anfnfang wird die Klasse deklariert 
 * Am Anfang des Methodenrumpfes werden die Attribute deklariert
 * 
 */
public class Raumschiff {
private int photonentorpedoAnzahl;
private int energieversorgungInProzent;
private int schildeInProzent;
private int huelleInProzent;
private int lebenserhaltungssystemeInProzent;
private int androidenAnzahl;
private String schiffsname;
private ArrayList<String>broadcastKommunikator;
private ArrayList<Ladung> ladungsverzeichnis;

/*
 * Der Konstruktor Raumschiff wird deklariert (nicht voll-paramterisiert)
 */
public Raumschiff() {
this.photonentorpedoAnzahl=0;
this.energieversorgungInProzent=0;
this.schildeInProzent=0;
this.huelleInProzent=0;
this.lebenserhaltungssystemeInProzent=0;
this.androidenAnzahl=0;
this.schiffsname="";
this.ladungsverzeichnis = new ArrayList<Ladung>();
this.broadcastKommunikator = new ArrayList<String>();
}
/*
 * Der Konstruktor Raumschiff deklariert (voll-paramterisiert)
 */
public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent, int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent,String schiffsname,int androidenAnzahl ) {
	
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	this.energieversorgungInProzent = energieversorgungInProzent;
	this.schildeInProzent = zustandSchildeInProzent;
	this.huelleInProzent = zustandHuelleInProzent;
	this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
	this.androidenAnzahl = androidenAnzahl;
	this.schiffsname = schiffsname;
	ladungsverzeichnis = new ArrayList<Ladung>();
	broadcastKommunikator = new ArrayList<String>();
}
public int getPhotonentorpedoAnzahl () {
	return photonentorpedoAnzahl;
}

public void setPhotonentorpedoAnzahl (int photonentorpedoAnzahl) {
	this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	
	
}

public int getEnergieversorgungInProzent() {
	return this.energieversorgungInProzent;
}

public void setEnergieversorgungInProzent(int zustandEnergieversorgungInProzentNeu ) {
	this.energieversorgungInProzent = zustandEnergieversorgungInProzentNeu;
}
public int getSchildeInProzent() {
	return schildeInProzent;
}
public void setSchildeInProzent(int schildeInProzent) {
	this.schildeInProzent = schildeInProzent;
}
public int getHuelleInProzent() {
	return huelleInProzent;
}
public void setHuelleInProzent(int huelleInProzent) {
	this.huelleInProzent = huelleInProzent;
}
public int getLebenserhaltungssystemeInProzent() {
	return this.lebenserhaltungssystemeInProzent;
}
public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
	this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
}
public int getAndroidenAnzahl() {
	return androidenAnzahl;
}
public void setAndroidenAnzahl(int AndroidenAnzahl) {
	this.androidenAnzahl = AndroidenAnzahl;
}
public String getSchiffsname() {
	return schiffsname;
}
public void setSchiffsname(String schiffsname) {
	this.schiffsname = schiffsname;
}

public void addLadung(Ladung neueladung) {
this.ladungsverzeichnis.add(neueladung);

}
/**
 * Die Methode zustandRaumschiff() gibt den Zustand bzw. Werte von den Attributen des Raumschiffes aus 
 * (Schiffsname, photonentorpedo, etc...)
 */
public void zustandRaumschiff() {
	System.out.println("Zustand des Raumschiffes: ");
	System.out.println("photonetorpedo: " + this.getPhotonentorpedoAnzahl());
	System.out.println("energieversorgungInProzent: "+ this.getEnergieversorgungInProzent());
	System.out.println("schildeInProzent: "+ this.getSchildeInProzent());
	System.out.println("huelleInProzent: "+ this.getHuelleInProzent());
	System.out.println("lebenserhaltungssystemeInProzent: " + this.getLebenserhaltungssystemeInProzent());
	System.out.println("schiffsname: "+ this.getSchiffsname());
	System.out.println("androidenAnzahl: " + this.getAndroidenAnzahl());
	System.out.println("\n\n");
}
/**
 * In der Methode photonentorpedoSchiessen() wird mithilfe von Kontrollstrukturen ueberprueft, wie hoch die Anzahl der Photonentorpedo ist.
 * Wenn die Anzahl 0 ist, dann wird als Ausgabe  -=*Click*=- ausgeben
 * Wenn denn die Zahl der Photonentorpedo kleiner 0 ist, dann wird die Anzahl gleich Null gesetzt
 * Andernfalls wird die Ausgabe "Photonentorpedo abgeschossen" an der Methode nachrichtAnAlle() mitgegeben
 * @param r int 
 */
public void photonentorpedoSchiessen(Raumschiff r){
	if (this.photonentorpedoAnzahl==0) {
		System.out.println("-=*Click*=-");
	}
	else if(this.photonentorpedoAnzahl<0 ) {
		this.photonentorpedoAnzahl = 0;
	}
	else{
		this.photonentorpedoAnzahl=this.photonentorpedoAnzahl-1;
		//System.out.println("Aktuelle Anzahl: "+ this.photonentorpedoAnzahl);
		nachrichtAnAlle("Photonentorpedo abgeschossen");
		treffer(r);
		}
}
/**
 * Bei der Methode phaserkanoneSchiessen() wird kontrolliert
 * wie der (aktuelle) Attributwert energieversorgungInProzent ist
 * der Wert unter 50, dann soll -=*Click*=- ausgegben, d.h. es passiert nichts.
 * Im anderen Fall wird die Ausgabe "Phaserkanone abgeschossen" an die Methode nachrichtAnAlle() weitergegben
 * und zum Anderem wird der Rueckgabeparamter r an die Methode treffer weitergegeben
 * Die Methode phaserkanoneSchiessen() 
 * @param r int
 */
public void phaserkanoneSchiessen(Raumschiff r) {
	if(this.energieversorgungInProzent <50) {
		System.out.println("-=*Click*=-");
	}
	else {
		this.energieversorgungInProzent = this.energieversorgungInProzent - 50;
		nachrichtAnAlle("Phaserkanone abgeschossen");
		treffer(r);
	}
}
/**
 * Bei der Methode treffer() wird der Attributwert von schildeInProzent mit 50 subtrahiert
 * Wenn die der Attributwert gleich 0 ist, dann soll der Attributwert fuer huelleInProzent und energieversorgungInProzent jeweils mit 50 subtrahiert
 * Andernfalls wenn jeweils die Werte schildeInProzent und huelleInProzent gleich 0 ist, dann wird
 * lebenserhaltungssystemeInProzent gleich 0 festgelegt und die String-Ausgabe an die Methode nachrichtAnAlle() weitergegben
 * Die private Methode gibt nichts aus (void)
 * @param r 
 */
private void treffer (Raumschiff r) {
	this.schildeInProzent = this.schildeInProzent-50;
	if (this.schildeInProzent ==0){
		this.huelleInProzent=this.huelleInProzent-50;
		this.energieversorgungInProzent = this.energieversorgungInProzent-50;
		
	}
	else if (this.schildeInProzent == 0 && this.huelleInProzent==0) {
		this.lebenserhaltungssystemeInProzent = 0;
		nachrichtAnAlle("Die Lebenserhaltungssysteme sind komplett vernichtet");
	}
	else{
		// a filler
	}
}
/**
 * In der Methode nachrichtAnAlle fuegt den Paramter message in die ArrayList broadcastKommunikator hinzugefuegt
 * @param message 
 */
public void nachrichtAnAlle(String message) {
	this.broadcastKommunikator.add(message);
}
/**
 * In der Methode eintraegeLogbuchZurueckgeben() wird die ArrayList broadcastKommunikator ausgegeben und die ArrayList als Rückgabewert zurück gegeben
 * 
 * @return 
 */
public ArrayList<String> eintraegeLogbuchZurueckgeben(){
	System.out.println(this.broadcastKommunikator);
	return this.broadcastKommunikator;
}
}



