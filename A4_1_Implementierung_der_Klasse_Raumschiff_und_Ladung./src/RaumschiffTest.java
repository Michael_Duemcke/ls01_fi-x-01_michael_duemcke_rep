import java.util.Scanner;
public class RaumschiffTest {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		
		Raumschiff raumschiffKlingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS'Hegtha", 2);
		Ladung ladungKlingonen_1 = new Ladung("FerengiSchneckensaft",200);
		Ladung ladungKlingonen_2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		Raumschiff raumschiffRomulaner = new Raumschiff(2,100,100,100,100,"IRW Khazara",2);
		Ladung ladungRomulaner_1 = new Ladung("Borg-Schrott",5);
		Ladung ladungRomulaner_2 = new Ladung("Rote Materie",2);
		Ladung ladungRomulaner_3 = new Ladung("Plasma-Waffe",50);
		
		Raumschiff raumschiffVulkanier = new Raumschiff(0,80,80,50,100, "Ni'var", 5);
		Ladung ladungVulkanier_1 = new Ladung("Forschungssonde",35);
		Ladung ladungVulkanier_2 = new Ladung ("Photonentorpedo",3);
		

		raumschiffKlingonen.photonentorpedoSchiessen(raumschiffRomulaner);
		raumschiffRomulaner.phaserkanoneSchiessen(raumschiffKlingonen);
		raumschiffVulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		
		raumschiffKlingonen.zustandRaumschiff();
		System.out.println(ladungKlingonen_1.toString());
		System.out.println(ladungKlingonen_2.toString());
		
		
		raumschiffKlingonen.photonentorpedoSchiessen(raumschiffRomulaner);
		raumschiffKlingonen.photonentorpedoSchiessen(raumschiffRomulaner);
		
		
		raumschiffKlingonen.zustandRaumschiff();
		System.out.println(ladungKlingonen_1.toString());
		System.out.println(ladungKlingonen_2.toString());
		raumschiffRomulaner.zustandRaumschiff();
		System.out.println(ladungRomulaner_1.toString());
		System.out.println(ladungRomulaner_2.toString());
		System.out.println(ladungRomulaner_3.toString());
		raumschiffVulkanier.zustandRaumschiff();
		System.out.println(ladungVulkanier_1.toString());
		System.out.println(ladungVulkanier_2.toString());
		
		raumschiffKlingonen.eintraegeLogbuchZurueckgeben();
		raumschiffRomulaner.eintraegeLogbuchZurueckgeben();
		raumschiffVulkanier.eintraegeLogbuchZurueckgeben();
		
	}

	
	

}
