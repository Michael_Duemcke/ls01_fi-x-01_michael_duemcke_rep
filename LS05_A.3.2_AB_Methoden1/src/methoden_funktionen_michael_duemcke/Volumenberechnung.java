package methoden_funktionen_michael_duemcke;
import java.util.Scanner;
public class Volumenberechnung {

	public static void main(String[] args) {
		// main area
		// print statement has taken the order of the task sheet
		Scanner my_Scanner= new Scanner (System.in);
		double zahl_a,zahl_b,zahl_c,zahl_h,zahl_r;
		
		System.out.print("Bitte geben Sie die L�nge des K�rpers ein\n");
		zahl_a= my_Scanner.nextDouble();
		
		System.out.print("Bitte geben Sie die Breite des K�rpers ein\n");
		zahl_b= my_Scanner.nextDouble();
		
		System.out.print("Bitte geben Sie die H�he des K�rpers ein\n");
		zahl_c= my_Scanner.nextDouble();
		
		System.out.print("Bitte geben Sie die H�he der Pyramide ein\n");
		zahl_h= my_Scanner.nextDouble();
		
		System.out.print("Bitte geben Sie den Radius des K�rpers ein\n");
		zahl_r= my_Scanner.nextDouble();
		
		System.out.println("volume of dice: "+ Dice(zahl_a));
		System.out.println("volume of quadar: "+ Quadar(zahl_a,zahl_b,zahl_c));
		System.out.println("volume of pyramide: "+ Pyramide( zahl_a, zahl_h ));
		System.out.println("volume of bowl: "+ Bowl(zahl_r));
	}
	
	public static double Dice(double a) {
		double dice;
		dice =a * a * a;
		return dice;
		
		
	}

	public static double Quadar(double a, double b, double c) {
		double quadar;
		quadar = a*b*c;
		return quadar;
		
		
	}
	public static double Pyramide(double a, double h) {
		double pyramide;
		pyramide = a*a*h/3;
		return pyramide;
		
	}
	public static double Bowl(double r) {
		final double PI = 3.14159265359;
		double bowl;
		bowl = 4/3 * Math.pow(r,3)*PI;
		return bowl;
	}
}
